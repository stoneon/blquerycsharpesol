﻿using Bloomberglp.Blpapi;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLquery
{
    class BLQuery
    {
        private static readonly Name FIELD_ID = new Name("id");
        private static readonly Name FIELD_MNEMONIC = new Name("mnemonic");
        private static readonly Name FIELD_DATA = new Name("fieldData");
        private static readonly Name FIELD_DESC = new Name("description");
        private static readonly Name FIELD_INFO = new Name("fieldInfo");
        private static readonly Name FIELD_ERROR = new Name("fieldError");
        private static readonly Name FIELD_MSG = new Name("message");

        int isDailyUpdate = 1;

        static void Main(string[] args)
        {
            //args = new string[] { "1", @"C:\projects\AC11Study\BLquery\BLquery\bin\Debug\inputbond.ini", "1" };
            BLQuery myQuery = new BLQuery();

            if (args.Count() <= 2)
            {
                Console.WriteLine("please enter the argument,which is in the following format\ntype[1:gen data; 2:gen diff] configpath\npause");
                Console.Read();
            }
            else
            {
                myQuery.ReadFieldDic();
                int type = Int32.Parse(args[0]);
                int isDailyUpdate = 0;
                if (type == 1)
                {
                    //myQuery.LoadDataDic("bbfields.tbl");
                    //get the queries and sent requests
                    //printf("%s", argv[1]);
                    if (args.Count() > 2)
                    {
                        isDailyUpdate = Int32.Parse(args[2]);
                        myQuery.isDailyUpdate = isDailyUpdate;
                    }

                    var requests = myQuery.ReadQueries(args[1]);
                    Console.WriteLine(string.Join("\n", requests));

                    myQuery.run(requests);
                }
                /*
                else if (type == 2)
                {
                    BOOL correct = myQuery.generateDiff(argv[2]);
                    if (correct == FALSE)
                    {
                        system("pause");
                    }
                }
                */
            }
            //Console.Read();
        }

        private void run(List<BLRequest> requests)
        {
            string serverHost = "localhost";
            int serverPort = 8194;

            SessionOptions sessionOptions = new SessionOptions();
            sessionOptions.ServerHost = serverHost;
            sessionOptions.ServerPort = serverPort;

            System.Console.WriteLine("Connecting to " + serverHost +
                ":" + serverPort);
            Session session = new Session(sessionOptions);
            bool sessionStarted = session.Start();
            if (!sessionStarted)
            {
                System.Console.Error.WriteLine("Failed to start session.");
                return;
            }
            if (!session.OpenService("//blp/refdata"))
            {
                System.Console.Error.WriteLine("Failed to open //blp/refdata");
                return;
            }
            Service refDataService = session.GetService("//blp/refdata");

            foreach (var blreq in requests)
            {
                Request request = null;
                if (blreq.requestType == 1)
                {
                    request = refDataService.CreateRequest("ReferenceDataRequest");

                }
                else if (blreq.requestType == 2)
                {
                    request = refDataService.CreateRequest("HistoricalDataRequest");
                    request.Set("startDate", blreq.startDate.ToString("yyyyMMdd"));
                    request.Set("endDate", blreq.endDate.ToString("yyyyMMdd"));
                    //request.Set("returnEids", true);
                }
                Element securities = request.GetElement("securities");
                foreach (var ticker in blreq.tickers)
                {
                    securities.AppendValue(ticker);
                }
                Element rfields = request.GetElement("fields");
                foreach (var field in blreq.fields)
                {
                    rfields.AppendValue(field);
                }
                System.Console.WriteLine("Sending Request: " + request);
                session.SendRequest(request, null);

                List<string> lines = new List<string>();
                while (true)
                {
                    try
                    {
                        Event eventObj = session.NextEvent();
                        foreach (Message msg in eventObj)
                        {
                            if (eventObj.Type != Event.EventType.RESPONSE &&
                                eventObj.Type != Event.EventType.PARTIAL_RESPONSE)
                            {
                                continue;
                            }

                            if (msg.HasElement("securityData"))
                            {
                                //Console.WriteLine(msg.AsElement);
                                Element sfields = msg.GetElement("securityData");

                                if (blreq.requestType == 1)
                                {
                                    int ne = sfields.NumValues;
                                    for (int j = 0; j < ne; j++)
                                    {
                                        string security = sfields.GetValueAsElement(j).GetElementAsString("security");
                                        //Console.WriteLine(security);
                                        Element field = sfields.GetValueAsElement(j).GetElement(FIELD_DATA);

                                        //Console.WriteLine(field);
                                        string line = security;
                                        foreach (var fi in blreq.fields)
                                        {
                                            if (field.HasElement(fi))
                                            {
                                                Element data = field.GetElement(fi);
                                                //Console.WriteLine(data);
                                                if (data.IsArray)
                                                {
                                                    line = "";
                                                    int de = data.NumValues;
                                                    for (int k = 0; k < de; k++)
                                                    {
                                                        lines.Add(security+","+data.GetValueAsElement(k).GetElementAsString("Security Description"));
                                                    }
                                                }
                                                else
                                                {
                                                    line += "," + field.GetElementAsString(fi);

                                                }
                                            }
                                            else
                                            {
                                                line += ",";
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(line))
                                            lines.Add(line);
                                    }
                                }

                                if (blreq.requestType == 2)
                                {
                                    if (sfields.HasElement(FIELD_DATA))
                                    {
                                        Element fields = sfields.GetElement(FIELD_DATA);
                                        int numElements = fields.NumValues;
                                        for (int i = 0; i < numElements; i++)
                                        {
                                            string line = "";
                                            var field = fields.GetValueAsElement(i);
                                            if (field.HasElement("date"))
                                            {
                                                line = field.GetElementAsDate("date").ToSystemDateTime().ToString("yyyyMMdd");
                                            }
                                            foreach (var fi in blreq.fields)
                                            {
                                                if (field.HasElement(fi))
                                                {
                                                    line += "," + field.GetElementAsString(fi);
                                                }
                                                else
                                                {
                                                    line += ",";
                                                }
                                            }
                                            lines.Add(line);
                                        }
                                    }
                                }
                            }
                        }
                        if (eventObj.Type == Event.EventType.RESPONSE)
                        {
                            File.WriteAllLines(blreq.outputpath, lines);
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine("Got Exception:" + ex);
                    }
                }
            }
        }

        //read the queries from the input file
        public List<BLRequest> ReadQueries(string inputPath)
        {
            List<BLRequest> reqs = new List<BLRequest>();
            DateTime now = DateTime.Today;
            DateTime yester = DateTime.Today.AddDays(-1);

            string buf = DateTime.Today.ToString("yyyyMMdd");
            //ts = localtime(&now);
            //strftime(buf, sizeof(buf), "%Y%m%d", ts);
            //ts = localtime(&yester);
            //strftime(yesterday, sizeof(yesterday), "%Y%m%d", ts);
            var lines = File.ReadAllLines(inputPath);
            for (int k = 1; k < lines.Count(); k++)
            {
                string line = lines[k];
                List<string> sub_List = new List<string>(); //the list of tickers/fields
                var cols = line.Split('\t');
                DateTime d;
                BLRequest request = new BLRequest();
                if (cols.Count() >= 7)
                {
                    reqs.Add(request);
                }
                for (int i = 1; i <= cols.Count(); i++)
                {
                    string item = cols[i - 1];

                    switch (i)
                    {
                        case 1: //1 or 2
                            if (Int32.Parse(item) == 1)
                            {
                                request.requestType = 1;
                            }
                            else if (Int32.Parse(item) == 2)
                            {
                                request.requestType = 2;

                            }
                            break;
                        case 2:
                            request.type = item;
                            break;
                        case 3: //first check if there is a file on this path
                            if (File.Exists(item))
                            {
                                //it is a path
                                var subs = File.ReadAllLines(item);
                                foreach (var sub in subs)
                                {
                                    var c = sub.Split(',');
                                    if (c.Count() > 1)
                                    {
                                        request.displaytickers.Add(c[0]);
                                        request.tickers.Add(c[1]);
                                    }
                                }
                            }
                            else
                            {
                                //it is direct input
                                request.displaytickers.AddRange(item.Split(',').ToList());
                                request.tickers.AddRange(item.Split(',').ToList());
                            }
                            break;
                        case 4:
                            var fcols = item.Split(',');
                            foreach (var c in fcols)
                            {
                                if (fieldDic.Keys.Contains(c))
                                {
                                    request.fields.Add(fieldDic[c]);
                                }
                                else
                                {
                                    request.fields.Add(c);
                                }
                            }

                            break;
                        case 5:
                            if (request.requestType == 2)
                            {
                                request.startDate = DateTime.ParseExact(item, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }

                            break;

                        case 6:
                            if (request.requestType == 2 && DateTime.TryParseExact(item, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                            {
                                request.endDate = d;
                            }

                            break;

                        case 7:
                            if (this.isDailyUpdate == 1)
                            {
                                request.outputpath = item + ".csv";
                                //sprintf(path, "%s.csv", item);
                            }
                            else if (this.isDailyUpdate == 3 || this.isDailyUpdate == 4)
                            {
                                request.outputpath = item;
                                //sprintf(path, "%s", item);
                            }
                            else
                            {
                                request.outputpath = item + buf + ".txt";
                                //sprintf(path, "%s%s.txt", item, buf);
                            }
                            break;
                    }
                }
            }
            return reqs;
        }
        Dictionary<string, string> fieldDic = new Dictionary<string, string>();
        public void ReadFieldDic()
        {
            //240|Futures|0| |064A|Put Implied Volatility|FUT_PUT_IMPLIED_VOLATILITY|1538|4|2
            var lines = File.ReadAllLines(@"bbfields.tbl");
            foreach (var line in lines)
            {
                var cols = line.Split('|');
                if (cols[1] != "Obsolete Mnemonic")
                    fieldDic.Add(cols[4], cols[6]);
            }


        }

    }

    class BLRequest
    {
        public int requestType = 1;
        public string type;
        public string outputpath;
        public DateTime startDate = DateTime.Today;
        public DateTime endDate = DateTime.Today;
        public List<string> tickers = new List<string>();
        public List<string> fields = new List<string>();
        public List<string> displaytickers = new List<string>();
        public override string ToString()
        {
            return requestType + " " + type + " " + string.Join(",", tickers) + " " + string.Join(",", fields) + " " + startDate.ToString("yyyyMMdd") + " " + endDate.ToString("yyyyMMdd") + " " + outputpath;
        }
    }

}
